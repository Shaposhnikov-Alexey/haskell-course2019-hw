module Main where

import Data.Text (Text)
import Text.Pretty.Simple (pPrint)
import Text.InterpolatedString.QM (qnb)

import AST
import Parser
import Text.Megaparsec

-- test1 = [qnb| 
--      while (ar != 10):
--      if (ar % 2 == 0): 
--          ar += 2
--      elif 
--          ar += 1
--      count += 1   
-- |]


-- main :: IO ()
-- main = do
--     pPrint $ parseMaybe whileParser input
--     where
--         input  = [qnb|
--             while (ar != 10):
--               if (ar % 2 == 0): 
--                   ar += 2
--               elif 
--                   ar += 1
--               count += 1   
--         |]


main :: IO ()
main = do
  parseTest whileParser "Person.__Age__"
  parseTest whileParser "Person.Age = 3"
  parseTest pyExpr "'str' + 'ing'"
  parseTest whileParser "drew.__dict__['swing']()"
  parseTest whileParser "drew.__getattribute__('battle_cry')()"
  
    --1 space - level of function, --2 space - level of if
  parseTest whileParser "def main(a, b):\n if a < 3 * b + 11:\n  b = 5\n  c = 32\n else:\n return a + b"
  parseTest whileParser "while 3 > 2: a = 3"
  parseTest whileParser "Person.getAge()"
  parseTest whileParser "a = types.MethodType(foo,drew)"
  parseTest pyExpr "a += 3"
  parseTest pyExpr "a * b"
  parseTest whileParser "a = lambda x: x * x"

  




{-
factorial = Function "Factorial" ["N"] 
			If ((Val $ PyTypes $ PyNum $ Int N) 'EQ' (Val $ PyTypes $ PyNum $ Int 1)) 
				(Return (Val $ PyTypes $ PyNum $ Int N))
			(Return (Multiply (Val $ PyTypes $ PyNum $ Int N) (Function "Factorial" 
				Subtract (Val $ PyTypes $ PyNum $ Int N) (Val $ PyTypes $ PyNum $ Int 1)])) 
-}

