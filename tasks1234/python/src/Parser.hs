{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Parser where


import           Data.Text (Text, unpack)
import           Control.Applicative            (empty)
import           Data.Char                      (isAlphaNum)
import           NeatInterpolation              ( text )
import           Control.Monad                  ( void )
import           Control.Monad.Combinators.Expr
import           Data.Void
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer    as L
import AST

type Parser = Parsec Void String

lineCmnt :: Parser ()
lineCmnt  = L.skipLineComment "//"
blockCmnt :: Parser ()
blockCmnt = L.skipBlockComment "/*" "*/"

scn :: Parser ()
scn = L.space space1 lineCmnt blockCmnt

sc :: Parser ()
sc = L.space (void $ takeWhile1P Nothing f) lineCmnt empty
  where
    f x = x == ' ' || x == '\t'

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: String -> Parser String
symbol = L.symbol sc

-- | 'parens' parses something between parenthesis.

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

bracers :: Parser a -> Parser a
bracers = between (symbol "[") (symbol "]")

quotes :: Parser a -> Parser a
quotes = between (symbol "'") (symbol "'")

-- | 'integer' parses an integer.

integer :: Parser Integer
integer = lexeme L.decimal

float :: Parser Double
float = lexeme L.float

-- | 'semi' parses a semicolon.

semi :: Parser String
semi = symbol ";"

comma :: Parser String
comma = symbol ","

rword :: String -> Parser ()
rword w = (lexeme . try) (string w *> notFollowedBy alphaNumChar)

rws :: [String] -- list of reserved words
rws =
  [ "if"   , "then"   , "else"   , "while"
  , "do"   , "skip"   , "true"   , "false"
  , "list"   , "["   , "!"   , "and"
  , "or"   , "def"   , "Class"   , "return"
  , "elif"   , "Call"   , "Void"   , "in"
  , "lambda"
  ]

identifier :: Parser String
identifier = (lexeme . try) (p >>= check)
 where
  p = (:) <$> (letterChar <|> char '_') <*> many (alphaNumChar <|> char '_')
  check x = if x `elem` rws
    then fail $ "keyword " ++ show x ++ " cannot be an identifier"
    else return x

fieldIndent :: Parser String
fieldIndent = (lexeme . try) $ do
  first <- identifier
  void (symbol ".")
  second <- identifier
  return (first ++ "." ++ second)

strIndent :: Parser String
strIndent = (lexeme . try) $ do
  name <- quotes identifier
  return name 

dynMetIndent :: Parser String
dynMetIndent = (lexeme . try) $ do
  first <- fieldIndent
  second <- (try $ bracers strIndent) <|> (try $ parens strIndent)  
  return (first ++ second)

whileParser :: Parser Stmt
whileParser = between sc eof stmt

stmt :: Parser Stmt
stmt = f <$> sepBy1 stmt' semi
  where
    -- if there's only one stmt return it without using ‘Seq’
      f [x] = x
      f xs  = Seq xs

stmt' :: Parser Stmt
stmt' = try whileStmt 
 <|> try ifStmt <|> try assignStmt
 <|> try assignTerm <|> try callFunc
 <|> try listPars <|> try (parens stmt)
 <|> try (bracers stmt) <|> try defFunk
 <|> try defClass <|> try lambFunc
 <|> try return' <|> try callMethod
 <|> try elseStmt <|> try callField

whileStmt :: Parser Stmt
whileStmt = do
  rword "while"
  cond <- pyExpr
  void (symbol ":")
  stmt1 <- stmt
  return (While cond stmt1)

defFunk :: Parser Stmt
defFunk = L.indentBlock scn p
  where
    p = do
      rword "def"
      name <- identifier
      arg <- parens (sepBy identifier comma)
      symbol ":"
      return (L.IndentMany Nothing (return . (Function name arg)) stmt)   

callFunc :: Parser Stmt
callFunc = do
  name1 <- identifier
  arg <- parens (sepBy pyExpr comma)
  return (CallFunc name1 arg)

lambFunc :: Parser Stmt
lambFunc = do
  rword "lambda"
  arg <- sepBy identifier comma
  void (symbol ":")
  body <- pyExpr
  return (LambdaFunc arg body) 

return' :: Parser Stmt
return' = do
  rword "return"
  expr <- pyExpr
  return (Return expr)

listPars :: Parser Stmt
listPars = do
  arg <- bracers (sepBy pyExpr comma)
  return (ListCon arg)

-- | Class Parsers ------------------------------------------------------

defClass :: Parser Stmt
defClass = do
  rword <- "Class"
  name <- identifier
  void <- symbol ":"
  state <- stmt
  return (Class name state)

callField :: Parser Stmt
callField = do
  name1 <- identifier
  void (symbol ".")
  name2 <- identifier
  return (Field name2 ("of class: " ++ name1))

callMethod :: Parser Stmt
callMethod = do
  name <- (try dynMetIndent) <|> fieldIndent
  arg <- parens (sepBy pyExpr comma)
  return (Method name arg)


-- | Dynamic  

-- | Loop and Conditions Parser -----------------------------

ifStmt :: Parser Stmt
ifStmt = L.indentBlock scn p
  where 
    p = do
     rword "if"
     cond <- pyExpr
     void (symbol ":")
     return (L.IndentSome Nothing (return . (If cond)) stmt)


elseStmt :: Parser Stmt
elseStmt = L.indentBlock scn p
   where
     p = do
       rword "else"
       void (symbol ":")
       return (L.IndentMany Nothing (return . (Else)) stmt)     

-- for == foreach in python
forStmt :: Parser Stmt
forStmt = do 
  rword "for"
  exp <- pyExpr
  rword "in"
  cont <- pyExpr 
  stmt1 <- stmt
  return (For exp cont stmt1)

-- | Assign Parser --------------------------------------------- 

assignTerm :: Parser Stmt
assignTerm = do
  var <-try fieldIndent <|> identifier
  void <- (symbol "=")
  expr <- pyExpr
  return (Assign var (Left expr))

assignStmt :: Parser Stmt
assignStmt = do
  var <- try fieldIndent <|> identifier 
  void <- (symbol "=")
  expr <- (try listPars 
    <|> try callFunc 
    <|> try lambFunc 
    <|> try callMethod
    <|> try callField)
  return (Assign var (Right expr))

pyExpr :: Parser PTerm
pyExpr = try (makeExprParser term eOperators) <|> term
    
eOperators :: [[Operator Parser PTerm]]
eOperators =
  [
      [Prefix (Not <$ rword  "not")]
    ,
    map InfixL
    [  
        BiOp AsMod <$ symbol "%="
      , BiOp Mod <$ symbol "%"
      , BiOp AsPow <$ symbol "**="
      , BiOp Pow <$ symbol "**"   
      ]
    ,
    map InfixL
    [   BiOp AsMul <$ symbol "*="
      , BiOp Mul <$ symbol "*"
      , BiOp AsDiv <$ symbol "/=" 
      , BiOp Div <$ symbol "/"    
    ]
    ,
    map InfixL
    [   BiOp AsAdd <$ symbol "+="
      , BiOp AsSub <$ symbol "-="
      , BiOp Add <$ symbol "+"
      , BiOp Sub <$ symbol "-"
      ]
    ,
    map InfixN 
    [   BiOp Eq  <$ symbol "=="
      , BiOp NotEq  <$ symbol "!="
      , BiOp LessEq  <$ symbol "<="
      , BiOp Less  <$ symbol "<" 
      , BiOp GreaterEq  <$ symbol ">="
      , BiOp Greater  <$ symbol ">"   
    ]
    ,

      [ InfixL (BiOp And <$ rword  "and")
      , InfixL (BiOp Or  <$ rword  "or" ) ]

    ]

term :: Parser PTerm
term = parens pyExpr
  <|> bracers pyExpr
  <|> try (Var <$> identifier)
  <|> try (FloatConst <$> float) <|> try (IntConst <$> integer)
  <|> try (StrConst <$> strIndent)
  <|> (BoolConst True <$ rword "true")
  <|> (BoolConst False <$ rword "false") 