module AST where

import Data.Void

data PTerm = Var String
          | IntConst Integer
          | FloatConst Double
          | BoolConst Bool
          | StrConst String
          | PyList [PTerm]
          | Neg PTerm
          | Not PTerm 
          | BiOp Op PTerm PTerm  
		        deriving (Show)

data Op = And | Or  
          | Less | Greater | Eq | NotEq
          | LessEq | GreaterEq  
          | Add | Sub | Mul | Div
          | Pow | Mod
          | AsAdd | AsSub | AsMul | AsDiv
          | AsPow | AsMod
            deriving (Show)


data Stmt = Seq [Stmt]
           | Assign String (Either PTerm Stmt)
           | If PTerm [Stmt]
           | Else [Stmt]
           | For PTerm PTerm Stmt
           | While PTerm Stmt
           | Function String [String] [Stmt]
           | CallFunc String [PTerm]
           | Method String [PTerm]
           | Field String String
           | Return PTerm
           | Class String Stmt
           | ListCon [PTerm]
           | LambdaFunc [String] PTerm
             deriving (Show)    
