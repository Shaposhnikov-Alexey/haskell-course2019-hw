# FORTH

## USAGE GUIDE

"1 ." - output top of the stack with word "."

"1 2 3 dup" - duplicates 3

"1 2 3 over" - copies 2 on top

"1 2 3 4 swap" - swaps 3 and 4

"1 2 3 rot" - rotates 1 2 3 <- to 2 3 1 <-

"1 2 tuck" - copies top value under the second one, 1 2 <- to 2 1 2 <- 

"10 0 do loop"

    loop from 10 to 0; takes two values from the stack, decrements first; 
    after encountering LOOP keyword compares them and either pushes them onto the 
    stack and returns to DO or continues execution

": fib 0 1 rot 0 do over + swap loop drop ;"

    example usage: "10 fib ."
    at each loop iteration the two top values of the stack are fibonacci
    numbers f2 f1; OVER + SWAP copies f2, takes f2 + f1 and pushes their sum,
    then swaps it with f2

": fac 0 1 1 2swap do tuck * swap 1 + loop drop ;"

    example usage: "1000 fac ."
    at each loop iteration counter and current factorial are on stack;
    tuck copies counter, "*" multiplies factorial and counter, "1 +" increments counter

": isZero 0 = if 1 . else 0 . then ;"

    example of conditionals, "=" checks two top values equality and prints the result (0/1)

# LOG

## 14/10/2019

Initial implementation of mini-forth
AST; Main contains example AST of a 
fibonacci program written in forth.

## 05/11/2019

Added parser, changed AST so that BinOp
is not a function on Stack, but a Char,
to avoid interpretation logic in parsing.

One issue is Def being inside Expr, which
makes it possible to build AST with word
definition inside another definition(not
something you expect from forth..)

## 12/11/2019

Provided some code examples with explanations 
and pretty-printing; added stack primitives to
AST.

## 24/11/2019

Uploaded interpreter, added repl to Main.
Primitives and binary operators are now one 
thing (VM modification) and are accessed from
primDict.
