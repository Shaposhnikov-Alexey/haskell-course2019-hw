module AST where

data AType = IType Int | CType Char deriving Show

data NumberOfState = NumberOfState Int deriving (Show, Eq)

data State = State Int deriving Show

data Transition = Transition Int Int AType | TransitionR Int Int AType Bool | TransitionS Int Int AType AType | TransitionRS Int Int AType Bool AType deriving Show
--R means Register, S means Stack, RS means Register with Stack

data FinalStates = FinalStates [State] deriving Show

data InitialStates = InitialStates [State] deriving Show

data Register = Register Int deriving Show

data Stack = Stack [AType] deriving Show

{-
Examle of automaton
1;
2;
3;
1 -> 3 [label='a'];
1 -> 2 [label='b'];
2 -> 3 [label='c'];
//1;
//3;

Accept strings: "a" and "bc"
-}