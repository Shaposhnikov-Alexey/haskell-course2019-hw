module Main where

import Data.Text (Text)
import Text.Pretty.Simple (pPrint)
import Text.InterpolatedString.QM (qnb)
import DataTypes
import Parser
import Interpreter
import Text.Megaparsec

main :: IO ()
--main = pPrint $ parseMaybe whileParser testMemCpy
--main = parseTest whileParser testFib
main = runRepl createEnv

{-
testFib = [qnb|
    int fib(int n) {
        if (n <= 1) {
            return n;
        }
        return (fib(n-1) + fib(n-2));
    }
|]

testMemCpy = [qnb|
    int* memcpy(int* destination, int* source, int num)
    {
    	int i = 0;
    	int* d = destination;
    	int* s = source;
    	int (i < num){
    	    *d = *s;
    	    ++d; ++s;
    	    ++i;
    	}
    	return destination;
    }
|]
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Fibonacci:
  MiniC>> int fib(int n) { if (n <= 1) { return n; } return (fib(n-1) + fib(n-2)); }
  
  MiniC>> print(fib(1), fib(2), fib(3), fib(4), fib(5));
  1, 1, 2, 3, 5
  
  MiniC>> fib(11);
  Returned: 89

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> MEMCPY:
  MiniC>> int* memcpy(int* destination, int* source, int num) { int i = 0; int* d = destination; int* s = source; while (i < num) { *d = *s; ++d; ++s; ++i; } return destination; }
  
  MiniC>> int* x = malloc*10; int* y = malloc*10; int j = 0; while (j < 10) { y[j] = j*j; ++j; }
  
  MiniC>> memcpy(x, y, 10);
  Returned: 1
  MiniC>>  int i = 0; while (i < 10) {print(x[i], '=', y[i]); ++i; }
  
  0, '=', 0
  1, '=', 1
  4, '=', 4
  9, '=', 9
  16, '=', 16
  25, '=', 25
  36, '=', 36
  49, '=', 49
  64, '=', 64
  81, '=', 81

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> POINTER SHOWCASE:
// we can see that indexing pointer (like y[0]) means getting offset from [y]'s index of memory value,
// so (ptr* d = y + i; y[i] == *d;) After while-loop Pointer [y] goes beyond allocated memory and points to int j

  MiniC>> j = 0; while (j < 10) { print(x[j], y[0]); ++y; ++j; }
  
  0, 0
  1, 1
  4, 4
  9, 9
  16, 16
  25, 25
  36, 36
  49, 49
  64, 64
  81, 81
  
  MiniC>> print(*y);
  10
  
  MiniC>> print(y, '=', &j); print(j);
  22, '=', 22
  
  10


>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> CORRECTNESS OF ENV AFTER FUNCTION:
  MiniC>> int x = 0; int y = 5; void fun(int x) {y = x; return; }
  
  MiniC>> fun(10); 
  Returned: (void)
  MiniC>> print(x, y);
  0, 10 

>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Functions typecheck:
  MiniC>> int sum(int a, int b) { return 'c'; }
  
  MiniC>> sum(1, 6);
  TypeMismatch "Wrong return type, expected [Integer]"
  MiniC>> bool fun(bool a, bool b) { return 1; }
  WrongReturn "Function type and return type must be equal"
  MiniC>> void fun2() { return true; }
  WrongReturn "Function type and return type must be equal"
  
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> MEMCPY ISSUES?:

  MiniC>> int* x = malloc*10; int j = 0; while (j < 10) { x[j] = j*j; ++j; }
  
  MiniC>> memcpy(x, x+3, 3);
  Returned: 1
  MiniC>>  print(x[0], x[1], x[2], x[3], x[4], x[5]);
  9, 16, 25, 9, 16, 25
  
  MiniC>> memcpy(x, x + 1, 3);
  Returned: 1
  MiniC>> print(x[0], x[1], x[2], x[3], x[4], x[5]);
  16, 25, 9, 9, 16, 25
  
-}

testFib = "int fib(int n) {\n if (n <= 1) {\n return n;\n }\n return (fib(n-1) + fib(n-2));\n }"

testMemCpy = "void* memcpy(void* destination, void* source, int num)\n {\n int i = 0;\n char* d = destination;\n char* s = source;\n  while (i < num) {\n *d = *s;\n ++d; ++s;\n ++i;\n }\n return destination;}"