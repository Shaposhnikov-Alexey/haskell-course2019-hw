## HW 1:
Ast implemented:
CType for types, CVal for Literals,
Expr for expressions, ExprOp and ExprOpUnary for operands (same for BoolExpr + relation operands)
Stmt, RetSmt and Declaration for basic constructions

## HW 2:
Done: 
Bit, logic and common operations (note that inc and dec are postfix there
but there are prefix inc and dec functions: ++var; --var;),
basic expressions, function and var declarations, 
different assignments, return, 
function calls, different types parsers,
correct nesting of brackets and curly braces,
pointers and array's indices
multiple print(...) expression (like print(&x, '=', *y);)

## HW 3:
Done:
REPL, Throwable types, Env
Declarations, type checking
Working with memory, allocations, indirections, addressing, pointers, setting and inserting into Env
Function, saving their args as discrete closure, correct substitute by indexing
Returning changed Env for Call Stmt (i.e. func(arg1, arg2,...);) and only value for expression (i.e. int x = sum(a, b);)
All operations <- evalExpr, evalBoolExpr
Work with memory list, allocating, searching for best-fit index, deleting obsolete allocated memory, ordered inserting

## P.S.: 
- This MiniC doesn't support string, though it can be arranged as pointer to array of chars
- And there is no typical array initialisation (like int a[10] = {1, 2, ..., 10}) - any array must be allocated:
  - int* x = malloc*size; x[0] = 1, x[1] = 2, ..., x[size] = size;
- Malloc also doesn't require pointing type (e.g. void *), 'cuz memory represents value per index
- And keep in mind that pointer's type determines values that can be received via
- After writing over 500 lines of code I realised that I chose rather inconvenient Env's type: 
  - it'd be easier and shorter to use 
 'data Env = Env (Map.Map Index (Either Variable Function)) (Map.Map String Index)', not to mention monad transformers but what's done is done
- Modular architecture of interpreter allows to easily modify it, so there are things that can be appended
- Chose REPL approach mostly for fun and something fresh

## TODO:
_