﻿{-# OPTIONS_GHC -fwarn-incomplete-patterns -fwarn-incomplete-uni-patterns #-}

module Interpreter where

import System.IO
import Text.Megaparsec (runParser)
import qualified Data.Map.Strict as Map
import Data.List
import Data.Bits       (shiftL, shiftR, (.&.), (.|.), xor)
import Debug.Trace

import DataTypes
import Parser          (whileParser)

-------------------------- Types part

type Index = Integer 
type Variable = (CType, [(CVal, Index)])
type Storage = Map.Map String Variable
  
type Function = (CType, Storage, [Stmt])

data Env = Env (Map.Map String (Either Variable Function)) [(Index, String)] -- list of used indices and owner's names

createEnv :: Env
createEnv = Env Map.empty []

-------------------------- IO part

proceed :: Throwable (Env, [Stmt]) -> IO ()
proceed (Left eErr)  = putStrLn (show eErr) >> runRepl (createEnv)
proceed (Right (env', list)) = do
  mapM_ (\res -> putStr (showStmt res)) list 
  putStrLn ""
  runRepl env' 
    
runRepl :: Env -> IO ()
runRepl env = do
  putStr "MiniC>> "
  hFlush stdout
  input <- getLine
  if input == ":q" then return () else
    case runParser whileParser "" input of
      Left pErr  -> putStrLn (show pErr) >> runRepl env
      Right stmt -> proceed $ evalProg (getBody stmt) env        
      
interpret :: String -> IO ()
interpret str = case runParser whileParser "" str of
  Left pErr  -> putStrLn (show pErr)
  Right stmt ->  proceed' $ evalProg (getBody stmt) createEnv
  
proceed' :: Throwable (Env, [Stmt]) -> IO ()
proceed' (Left eErr)  = putStrLn (show eErr) 
proceed' (Right (env', list)) = mapM_ (\res -> putStrLn (showStmt res)) list 


-------------------------- Evaluation part

evalProg :: [Stmt] -> Env -> Throwable (Env, [Stmt])
evalProg [] env     = Right (env, [])
evalProg [stmt] env = do 
  (env, out) <- evalStmt stmt env 
  return (env, [out])
evalProg (stmt:other) env = do
  (env1, output1) <- evalStmt stmt env 
  case output1 of 
    Ret _                     -> return (env1, [output1])
    Block rt@[(Ret _)]        -> return (env1, rt)
    Block rt@(_:((Ret _):[])) -> return (env1, rt)
    _                         -> do
      (env2, output2) <- evalProg other env1
      return (env2, output1 : output2)

evalStmt :: Stmt -> Env -> Throwable (Env, Stmt)
evalStmt stmt env@(Env mp list) = case stmt of 
  Assign var@(Var name) expr@(Memory _)  -> case Map.lookup name mp of
    Nothing                 -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
    Just (Left (typ, vals)) -> declare (Var' typ name expr) env
    Just _                  -> Left (TypeMismatch "You can only allocate memory for pointers")
  Assign var@(Var name) e -> do
    cval <- evalExpr e env
    setVar name CNull cval env
  Assign var@(VarIndexed (name, indx)) e -> do
    cval  <- evalExpr e env 
    indx' <- evalExpr indx env 
    setVar name indx' cval env
  Assign _ _ -> Left (WrongExpr "Only variables can be assigned")
  
  IfElse cond st alt -> do
    cond' <- evalBoolExpr cond env
    if cond' then
      evalStmt st env
    else 
      evalStmt alt env
  If cond st         -> do
    cond' <- evalBoolExpr cond env
    if cond' then
      evalStmt st env
    else 
      return (env, Block [])
      
  While cond st -> do
    cond' <- evalBoolExpr cond env
    if cond' then do
      (env2, out) <- evalStmt st env
      case out of 
        rt@(Block [(Ret _)])        -> return (env2, rt)
        rt@(Block (_:((Ret _):[]))) -> return (env2, rt)
        _                           -> do
          (env', stmt) <- evalStmt (While cond st) env2
          case stmt of
            Block out' -> return (env', (Block $ out : out'))
            _          -> Left (Default "`While` must return Block of statements") --not reachable
    else 
      return (env, Block [])

  Decl decl -> declare decl env
  Ret smth  -> case smth of 
    Returned val -> return (env, Ret $ Returned val)
    RVoid        -> return (env, Ret $ Returned CVoid)
    RBExpr bexpr -> do
      bool <- evalBoolExpr bexpr env
      return (env, Ret $ Returned (CBool bool))
    RExpr expr   -> do
      val <- evalExpr expr env
      return (env, Ret $ Returned val)
    
  FCall e@(Call name args) -> case Map.lookup name mp of
    Nothing          -> Left (NotInScope $ "Function [" ++ name ++ "] is not defined")
    Just (Left var)  -> Left (NotInScope $ "[" ++ name ++ "] is variable, not a function")
    Just (Right (typ, closure, body)) -> do
      (env', retVal) <- invoke typ closure body args env
      return (env', Ret $ Returned retVal)
  FCall _ -> Left (TypeMismatch "Only functions can be called")
  
  Print []       -> return (env, Print [])
  Print ([expr]) -> do
    val <- evalExpr expr env
    return (env, Print [(Literal val)])
  Print (expr:oth) -> do
    (env1, stmt) <- evalStmt (Print oth) env
    case stmt of 
      Print vals -> do
        val <- evalExpr expr env1
        return (env1, Print ((Literal val):vals))
      _          -> Left (TypeMismatch "You can only print values")
      
  Block list -> do
    (newEnv, out) <- evalProg list env
    return (newEnv, Block out)
  
declare :: Declaration -> Env -> Throwable (Env, Stmt)
declare (Var' typ@(CTPtr t) name (Memory sizeExpr)) env@(Env mp list) = do
  val <- evalExpr sizeExpr env
  case val of
    CInt size 
      | (size <= 0) -> Left (Default "Allocation size should be more than 0")
      | otherwise   -> allocate typ name size env
    _         -> Left (TypeMismatch "Expected number for allocation's size")
--declare (Var' typ@(CTPtr _) _ _) _ = Left (TypeMismatch "Pointers can only be assigned to allocated memory")
    
declare (Var' typ name expr) env = do
  cval <- evalExpr expr env
  case cval of
    CInt num -> case typ of
       CTInt          -> insertVar typ name cval env
       CTPtr ptr      -> insertVar typ name cval env
       otherwise      -> Left (TypeMismatch $ "Can't assign integer to [" ++ show typ ++ "] type")
    CChar ch
      | (typ == CTChar) -> insertVar typ name cval env
      | otherwise       -> Left (TypeMismatch $ "Can't assign char to [" ++ show typ ++ "] type")
    CBool bl
      | (typ == CTBool) -> insertVar typ name cval env
      | otherwise       -> Left (TypeMismatch $ "Can't assign bool to [" ++ show typ ++ "] type")
    _        -> Left (TypeMismatch $ "Can't assign [" ++ show cval ++ "] to [" ++ show typ  ++ "] type")
    
declare (Func typ name args body) env@(Env mp list) = case Map.lookup name mp of 
  Nothing -> do
    retType <- getRetType (getBody body)
    if (retType /= typ) && ((typ == CTBool) || (typ == CTVoid)) then 
      Left (WrongReturn "Function type and return type must be equal")
    else do
      let closure = createClosure args
      let env' = Env (Map.insert name (Right (typ, closure, (getBody body))) mp) list
      return (env', Decl (Func typ name args body))
  _ -> Left (Redefine $ "[" ++ name ++ "] is already defined")
declare _ _ = Left (TypeMismatch "Wrong type for declaration")

evalExpr :: Expr -> Env -> Throwable CVal
evalExpr expr env@(Env mp list) = case expr of
  Literal val           -> return val
  Var name              -> case Map.lookup name mp of
    Nothing                     -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
    Just (Left (t, [(val, i)])) -> return val
    _                           -> Left (Default $ "Incorrect [" ++ name ++ "] call")
  VarIndexed (name, ex) -> case Map.lookup name mp of
    Nothing                     -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
    Just (Left (typ, vals))     -> case typ of 
      CTPtr ptr -> getIndexedVal (getReferred vals env) ex env
      _         -> getIndexedVal vals ex env
    _                           -> Left (Default $ "Incorrect [" ++ name ++ "] call")
  Neg expr              -> do
    val <- evalExpr expr env
    case val of
      CInt num -> return (CInt $ -num)
      CBool bl -> return (CBool (not bl))
      _        -> Left (TypeMismatch "Incorrect type for negation")
  Call name args        -> case Map.lookup name mp of
    Nothing          -> Left (NotInScope $ "Function [" ++ name ++ "] is not defined")
    Just (Left var)  -> Left (NotInScope $ "[" ++ name ++ "] is variable, not a function")
    Just (Right (typ, closure, body)) -> do
      (env', retVal) <- invoke typ closure body args env 
      return retVal
  Memory expr           -> Left (TypeMismatch "Memory allocation can only be used at assignment")
  ExprBinary bOp e1 e2  -> do
    val1 <- evalExpr e1 env
    val2 <- evalExpr e2 env 
    calcBin bOp val1 val2 env
  ExprUnary uOp expr    -> calcUn uOp expr env

calcBin :: ExprOp -> CVal -> CVal -> Env -> Throwable CVal
calcBin Add (CInt num1) (CInt num2) env = return (CInt $ num1 + num2)
calcBin Add _ _ _ = Left (TypeMismatch "Operation Add (+) suppose 2 integers as arguments")

calcBin Subtract (CInt num1) (CInt num2) env = return (CInt $ num1 - num2)
calcBin Subtract _ _ _ = Left (TypeMismatch "Operation Subtract (-) suppose 2 integers as arguments")

calcBin Multiply (CInt num1) (CInt num2) env = return (CInt $ num1 * num2)
calcBin Multiply _ _ _ = Left (TypeMismatch "Operation Multiply (*) suppose 2 integers as arguments")

calcBin Divide _ (CInt 0) _ = Left (Default "Operation Divide (/) expects non-zero divisor")
calcBin Divide (CInt num1) (CInt num2) env = return (CInt $ quot num1 num2) 
calcBin Divide _ _ _ = Left (TypeMismatch "Operation Divide (/) suppose 2 integers as arguments")

calcBin Mod (CInt num1) (CInt num2) env = return (CInt $ rem num1 num2)
calcBin Mod _ _ _ = Left (TypeMismatch "Operation Mod (%) suppose 2 integers as arguments")

calcBin BitShl (CInt num1) (CInt num2) env = return (CInt (shiftL num1 $ fromInteger num2))
calcBin BitShl _ _ _ = Left (TypeMismatch "Operation Bit (<<) suppose 2 integers as arguments")

calcBin BitShr (CInt num1) (CInt num2) env = return (CInt (shiftR num1 $ fromInteger num2))
calcBin BitShr _ _ _ = Left (TypeMismatch "Operation Bit (>>) suppose 2 integers as arguments")

calcBin BitAnd (CInt num1) (CInt num2) env = return (CInt $ num1 .&. num2)
calcBin BitAnd _ _ _ = Left (TypeMismatch "Operation Bit (&) suppose 2 integers as arguments")

calcBin BitOr (CInt num1) (CInt num2) env = return (CInt $ num1 .|. num2)
calcBin BitOr _ _ _ = Left (TypeMismatch "Operation Bit (|) suppose 2 integers as arguments")

calcBin BitXor (CInt num1) (CInt num2) env = return (CInt $ num1 `xor` num2)
calcBin BitXor _ _ _ = Left (TypeMismatch "Operation Bit (^) suppose 2 integers as arguments")

calcUn :: ExprOpUnary -> Expr -> Env -> Throwable CVal
calcUn Inc (Var name) env@(Env mp list) = do
  val <- getVal name (Literal CNull) env
  case val of
    CInt num -> do
      let out = setVar name CNull (CInt $ num + 1) env 
      return $ (CInt $ num + 1)
    _        -> Left (TypeMismatch "Wrong type for increment")
calcUn Inc (VarIndexed (name, indx)) env@(Env mp list) = case Map.lookup name mp of
  Nothing                 -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
  Just (Left (typ, vals)) -> do
    val <- getIndexedVal (getReferred vals env) indx env
    case val of
      CInt num -> do
        cval <- evalExpr indx env
        let out = setVar (getRefName vals env) cval (CInt $ num + 1) env 
        return $ (CInt $ num + 1)
      _        -> Left (TypeMismatch "You can only increment integers")
  _                       -> Left (TypeMismatch "Wrong type for increment")
calcUn Inc _ _ = Left (TypeMismatch "Only variables can be incremented")

calcUn Dec (Var name) env@(Env mp list) = do
  val <- getVal name (Literal CNull) env
  case val of
    CInt num -> do
      let out = setVar name CNull (CInt $ num - 1) env 
      return $ (CInt $ num - 1)
    _        -> Left (TypeMismatch "Wrong type for decrement")
calcUn Dec (VarIndexed (name, indx)) env@(Env mp list) = case Map.lookup name mp of
  Nothing                 -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
  Just (Left (typ, vals)) -> do
    val <- getIndexedVal (getReferred vals env) indx env
    case val of
      CInt num -> do
        cval <- evalExpr indx env
        let out = setVar (getRefName vals env) cval (CInt $ num - 1) env 
        return $ (CInt $ num - 1)
      _        -> Left (TypeMismatch "You can only increment integers")
  _                       -> Left (TypeMismatch "Wrong type for increment")
calcUn Dec _ _ = Left (TypeMismatch "Only variables can be decremented")

calcUn Indirection (Var name) env@(Env mp list) = case Map.lookup name mp of
  Nothing                 -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
  Just (Left (typ, vals)) -> case typ of 
    CTPtr ptr -> do
      val <- getIndexedVal vals (Literal $ CInt 0) env
      case val of
        CInt num -> getPtrVal num env
        _        -> Left (Default "Pointer has non-integer value, it's incorrect")
    _         -> Left (TypeMismatch "Wrong type for indirection")
  _                       -> Left (WrongExpr $ "[" ++ name ++ "] is function, can't be expression")
calcUn Indirection _ _ = Left (TypeMismatch "Indirection can only be applied to pointers")

calcUn Address (Var name) env@(Env mp list) = case Map.lookup name mp of
  Nothing                 -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
  Just (Left (typ, vals)) -> do
    num <- getIndexOfVal vals (Literal $ CInt 0) env
    return $ CInt num
  _                       -> Left (WrongExpr $ "[" ++ name ++ "] is function, can't be expression")
calcUn Address (VarIndexed (name, indx)) env@(Env mp list) = case Map.lookup name mp of
  Nothing                 -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
  Just (Left (typ, vals)) -> case typ of 
    CTPtr ptr -> do
      num <- getIndexOfVal (getReferred vals env) indx env
      return $ CInt num
    _         -> do
      num <- getIndexOfVal vals indx env
      return $ CInt num
  _                       -> Left (WrongExpr $ "[" ++ name ++ "] is function, can't be expression")
calcUn Address _ _ = Left (TypeMismatch "Address can only be applied to variables")

evalBoolExpr :: BoolExpr -> Env -> Throwable Bool
evalBoolExpr (BoolConst bl) _ = return bl
evalBoolExpr (Not bl) env     = do
  bool <- evalBoolExpr bl env 
  return (not bool)
evalBoolExpr (BoolExpr expr) env = do 
  val <- evalExpr expr env
  case val of
    CInt num -> if (num /= 0) then 
                          return True
                          else return False
    _        ->  Left (TypeMismatch "Bool expression should contain either bool or number")
evalBoolExpr (BBinary op b1 b2) env = do
  val1 <- evalBoolExpr b1 env
  val2 <- evalBoolExpr b2 env
  case op of
    And -> return (val1 && val2)
    Or  -> return (val1 || val2)
evalBoolExpr (RBinary op e1 e2) env = do
  val1 <- evalExpr e1 env
  val2 <- evalExpr e2 env
  evalRelation op val1 val2 env

evalRelation :: RelationBinOp -> CVal -> CVal -> Env -> Throwable Bool
evalRelation Equal val1 val2 env = return (val1 == val2)
evalRelation NotEq val1 val2 env = return (val1 /= val2)
evalRelation op (CInt num1) (CInt num2) env =  case op of
  Greater -> return (num1 > num2)
  Less    -> return (num1 < num2)
  GtEq    -> return (num1 >= num2)
  LtEq    -> return (num1 <= num2)
  _       -> Left (Default "Isn't supposed to reach there")
evalRelation _ _ _ _ = Left (TypeMismatch "(>), (<), (>=) and (<=) can only be applied to integers")

invoke :: CType -> Storage -> [Stmt] -> [Expr] -> Env -> Throwable (Env, CVal)
invoke typ strg block args env@(Env mp list) = 
  if (Map.size strg) == (length args) then do
    locEnv                <- substitute (storageToEnv strg) args env
    (funcEnv, out)        <- evalProg block (Env (Map.union locEnv mp) list)
    ((Env mp1 ls1), rval) <- clearUp funcEnv out 
    let changedEnv = Map.intersection mp1 mp
    let overEnv    = Map.intersection mp locEnv
    let neatEnv    = Map.union overEnv changedEnv
    case rval of 
      CInt num -> case typ of
        CTInt   -> return (Env neatEnv ls1, rval) 
        CTPtr x -> return (Env neatEnv ls1, rval)
        _       -> Left (TypeMismatch $ "Wrong return type, expected [" ++ show typ ++ "]")
      CChar ch
        | typ == CTChar -> return (Env neatEnv ls1, rval) 
        | otherwise     -> Left (TypeMismatch $ "Wrong return type, expected [" ++ show typ ++ "]")
      CBool bl
        | typ == CTBool -> return (Env neatEnv ls1, rval) 
        | otherwise     -> Left (TypeMismatch $ "Wrong return type, expected [" ++ show typ ++ "]")
      CVoid 
        | typ == CTVoid -> return (Env neatEnv ls1, rval) 
        | otherwise     -> Left (TypeMismatch $ "Wrong return type, expected [" ++ show typ ++ "]")
      _ -> Left (TypeMismatch $ "Wrong return type, expected [" ++ show typ ++ "]")
  else
    Left (WrongNumArg "Incorrect number of arguments for function call")

clearUp :: Env -> [Stmt] -> Throwable (Env, CVal)
clearUp messedEnv@(Env mp ls) ((Ret (Returned val)):_) = 
  return ((Env (Map.fromList $ clearEnv (Map.toList mp)) ls), val)
    where 
      clearEnv [] = []
      clearEnv (v1@(name, (Left (typ, [(val, ind)]))):oth) 
        | (ind < 0)    = clearEnv oth
        | otherwise    = v1 : (clearEnv oth)
      clearEnv (another:oth) = another : (clearEnv oth)
clearUp messedEnv (smth:oth) = clearUp messedEnv oth --to get to final output's Stmt 
clearUp _ _                  = Left (WrongReturn "Function must return any value")

-------------------------- Working with Env part

insertVar :: CType -> String -> CVal -> Env -> Throwable (Env, Stmt)
insertVar typ name val env@(Env mp list) = case Map.lookup name mp of 
  Nothing -> do
    let indx = getAnyIndex list
    let env' = Env (Map.insert name (Left (typ, [(val, indx)])) mp) (insIndices indx name 1 list)
    return (env', Decl (Var' typ name (Literal val)))
  _ -> Left (Redefine $ "[" ++ name ++ "] is already defined")
  
setVar :: String -> CVal -> CVal -> Env -> Throwable (Env, Stmt) 
setVar name indx val env@(Env mp list) = case Map.lookup name mp of
  Nothing                 -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
  Just (Left (typ, vals)) -> case val of
    CInt num 
      | (typ == CTInt) || ((typ == CTPtr CTInt) && (indx == CNull)) -> do
          newVals <- changeVal vals indx val
          let env' = Env (Map.insert name (Left (typ, newVals)) mp) list 
          return (env', (Assign (Var name) (Literal val)))
      | (typ == CTPtr CTInt) -> case indx of 
          CInt num  -> set'' vals num val CTInt env
          CPtr      -> set'' vals 0 val CTInt env
          otherwise -> Left (TypeMismatch "Index must be numerical")
      | otherwise       -> Left (TypeMismatch $ "Can't assign integer to [" ++ show typ ++ "] type")
{-    CNull
      | (typ == CTInt) || ((typ == CTPtr CTInt) && (indx == CNull)) -> do
                newVals <- changeVal vals indx val
                let env' = Env (Map.insert name (Left (typ, newVals)) mp) list 
                return (env', (Assign (Var name) (Literal val)))
      | (typ == CTPtr CTInt)   -> case indx of 
          CInt num  -> set'' vals num val CTInt env
          CPtr      -> set'' vals 0 val CTInt env
          otherwise -> Left (TypeMismatch "Index must be numerical")
      | otherwise       -> Left (TypeMismatch $ "Can't assign integer to [" ++ show typ ++ "] type")
-}
-- it depends on whether Null is eligible to assign
    CChar ch 
      | (typ == CTChar) || ((typ == CTPtr CTChar) && (indx == CNull)) -> do
          newVals <- changeVal vals indx val
          let env' = Env (Map.insert name (Left (typ, newVals)) mp) list 
          return (env', (Assign (Var name) (Literal val)))
      | (typ == CTPtr CTChar) -> case indx of 
          CInt num  -> set'' vals num val CTChar env
          CPtr      -> set'' vals 0 val CTChar env
          otherwise -> Left (TypeMismatch "Index must be numerical")
      | otherwise       -> Left (TypeMismatch $ "Can't assign char to [" ++ show typ ++ "] type")
    CBool bl
      | (typ == CTBool) || ((typ == CTPtr CTBool) && (indx == CNull)) -> do
          newVals <- changeVal vals indx val
          let env' = Env (Map.insert name (Left (typ, newVals)) mp) list 
          return (env', (Assign (Var name) (Literal val)))
      | (typ == CTPtr CTBool)  -> case indx of 
          CInt num  -> set'' vals num val CTBool env
          CPtr      -> set'' vals 0 val CTBool env
          otherwise -> Left (TypeMismatch "Index must be numerical")
      | otherwise       -> Left (TypeMismatch $ "Can't assign bool to [" ++ show typ ++ "] type")
    _ -> Left (TypeMismatch $"Wrong type for assignment: [" ++ show val ++ "]")
  _ -> Left (TypeMismatch $ "Can't assign to function [" ++ name ++ "]")
-- there are too much variables to get rid of copy-paste

set'' :: [(CVal, Index)] -> Integer -> CVal -> CType -> Env -> Throwable (Env, Stmt)
set'' vals num val typ env@(Env mp list) = do
  newVals <- changeReferred vals num val env
  let name' = getRefName vals env
  let env' = Env (Map.insert name' (Left (typ, newVals)) mp) list
  return (env', (Assign (Var name') (Literal val)))

allocate :: CType -> String -> Integer -> Env -> Throwable (Env, Stmt)
allocate ptr@(CTPtr typ) name size env@(Env mp ls) = case Map.lookup name mp of
  Nothing -> do
    let ptrIndx = getAnyIndex ls
    let indx    = getSuitableIndex (insertOrdered ptrIndx name ls) size
    (env1@(Env mp1 ls1), _) <- insertVar ptr name (CInt indx) env
    let env2 = Env (Map.insert ("ptr_" ++ name) (Left (typ, (zeroList indx size))) mp1) 
                   (insIndices indx ("ptr_" ++ name) size ls1)
    return (env2, Decl (Mem name))
  Just (Left (typ', vals)) 
    | (typ /= typ') -> Left (TypeMismatch $ "Can't reassign [" ++ name ++ "], as types mismatch")
    | otherwise     -> do
       let env' = Env (Map.delete name mp) (delIndices vals ls) 
       allocate typ name size env'
  _                 -> Left (Redefine $ "Name [" ++ name ++ "] is already defined")
allocate _ _ _ _ = Left (TypeMismatch "Only pointers can be assigned to memory allocation")

createClosure :: [(CType, String)] -> Storage
createClosure args = fill args (Map.empty) (-1) where
  fill []                  clsr indx = clsr 
  fill [(typ, name)]       clsr indx = Map.insert name (typ, [(CNull, indx)]) clsr   
  fill ((typ, name):other) clsr indx = Map.insert name (typ, [(CNull, indx)]) (fill other clsr (indx - 1))
  
storageToEnv :: Storage -> (Map.Map String (Either Variable Function))
storageToEnv strg = case Map.toList strg of
  []   -> Map.empty
  list -> Map.fromList $ helper list
    where 
      helper [] = []
      helper ((name, var):oth) = (name, Left var) : (helper oth)
     
substitute :: (Map.Map String (Either Variable Function))-> [Expr] -> Env 
              -> Throwable (Map.Map String (Either Variable Function)) 
substitute strg args env = do
  list <- helperSub (Map.toList strg) args env (-1)
  return $ Map.fromList list
    
helperSub :: [(String, Either Variable Function)] -> [Expr] -> Env 
             -> Index -> Throwable [(String, Either Variable Function)]
helperSub list [] _ _ = return list -- remember that args' number == list's size
helperSub list (arg:othArg) env pos = do
  subsVar <- helperSubArg list arg env pos
  varList <- helperSub list othArg env (pos - 1)
  return $ varList ++ [subsVar]
    
helperSubArg :: [(String, Either Variable Function)] -> Expr -> Env 
             -> Index -> Throwable (String, Either Variable Function)
helperSubArg [] arg _ _ = Left (WrongNumArg "Couldn't substitute arguments to function's closure")
helperSubArg ((name, Left (typ, [(oldVal, indx)])):oth) arg env pos 
  | (indx == pos) = do
      newVal <- evalExpr arg env
      case newVal of
        CInt n -> case typ of 
          CTInt     -> return (name, (Left (typ, [(newVal, pos)])))
          CTPtr x   -> return (name, (Left (typ, [(newVal, pos)])))
          otherwise -> Left (TypeMismatch $ "Expected [" ++ show typ ++ "] type for argument")
        CChar ch 
          | (typ == CTChar) -> return (name, (Left (typ, [(newVal, pos)])))
          | otherwise       -> Left (TypeMismatch $ "Expected [" ++ show typ ++ "] type for argument")
        CBool b  
          | (typ == CTBool) -> return (name, (Left (typ, [(newVal, pos)])))
          | otherwise       -> Left (TypeMismatch $ "Expected [" ++ show typ ++ "] type for argument")
        otherwise                    -> Left (TypeMismatch $ "Incorrect type of argument")
  | otherwise = helperSubArg oth arg env pos 
helperSubArg _ _ _ _ = Left (Default "Error involving function call")

getRetType :: [Stmt] -> Throwable CType
getRetType []        = Left (WrongReturn "Function must contain expressions") 
getRetType ([stmt]) = case stmt of
  Ret RVoid          -> return CTVoid
  Ret (RBExpr e)     -> return CTBool
  Ret (RExpr e)      -> return RetExpr
  If cond stmt       -> getRetType (getBody stmt)
  IfElse cond st alt -> getRetType (getBody st) -- alt is not checked but however
  _                  -> Left (WrongReturn "Function must contain 'return' expression")
getRetType stmts@(st:other) = getRetType (tail stmts)

getBody :: Stmt -> [Stmt]
getBody (Block list) = list
getBody stmt         = [stmt]

getVal :: String ->  Expr -> Env -> Throwable CVal
getVal name indx env@(Env mp list) = case Map.lookup name mp of
  Nothing                 -> Left (NotInScope $ "Variable [" ++ name ++ "] is not defined")
  Just (Left (typ, vals)) -> getIndexedVal vals indx env
  _                       -> Left (Redefine $ "[" ++ name ++"] is defined as function")

changeReferred :: [(CVal, Index)] -> Index -> CVal -> Env -> Throwable [(CVal, Index)]
changeReferred vals@[(CInt indx, _)] n nVal env@(Env mp ls) = case Map.lookup (getRefName vals env) mp of
  Nothing                  -> Left (OutOfRange "Wrong address called")
  Just (Left (typ, vals')) -> helperChange vals' (indx + n) nVal
    where 
      helperChange [] _ _ = Left (OutOfRange "Address index is incorrect")
      helperChange vals@((val, indx):other) n nVal 
        | (n == indx) = return $ (nVal, indx):other
        | (n > indx)  = do
            list <- helperChange other n nVal
            return $ (val, indx):list
        | otherwise   = Left (OutOfRange "Address index is incorrect")
  _ -> Left (TypeMismatch "Can't refer to functions")
changeReferred _ _ _ _ = Left (TypeMismatch "Pointer should only contain address")

getReferred :: [(CVal, Index)] -> Env -> [(CVal, Index)] 
getReferred vals@[(CInt indx, _)] env@(Env mp list) = case Map.lookup (getRefName vals env) mp of
  Nothing                 -> error "Segmentation fault"
  Just (Left (typ, vals)) -> helperRef vals indx
    where
      helperRef vals@((val, indx):other) n 
        | (n == indx) = vals
        | (n > indx)  = helperRef other n
        | otherwise   = error $ "Segmentation fault" 
      helperRef [] _ = error "Segmentation fault"
  _                       -> error "Segmentation fault"
getReferred _ _ = error "Segmentation fault" 

getRefName :: [(CVal, Index)] -> Env -> String 
getRefName vals@[((CInt indx), _)] env@(Env mp list) = case getNameAddr indx list of
  Left _     -> error "Segmentation fault"
  Right name -> name
getRefName _ _ = error "Segmentation fault"  
 
getPtrVal :: Index -> Env -> Throwable CVal
getPtrVal indx env@(Env mp list) = do
  name <- getNameAddr indx list
  case Map.lookup name mp of
    Nothing                            -> Left (NotInScope "Pointer list is broken")
    Just (Left (t, vals@((v, s):oth))) -> getIndexedVal vals (Literal $ CInt (indx - s)) env 
    _                                  -> Left (Redefine $ "[" ++ name ++"] is defined as function") --unreachable
    
getNameAddr :: Index -> [(Index, String)] -> Throwable String
getNameAddr indx [] = Left (OutOfRange $ "Address [" ++ show indx ++ "] isn't provided")
getNameAddr indx ((a, name):oth)
  | (indx == a) = return name
  | (indx > a)  = getNameAddr indx oth
  | otherwise   = Left (OutOfRange $ "Address [" ++ show indx ++ "] isn't provided")
  
-------------------------- Indices list part

zeroList :: Index -> Integer -> [(CVal, Index)]
zeroList indx size = map (\x->(CNull, x)) [indx..(indx + (size - 1))]

delIndices :: [(CVal, Index)] -> [(Index, String)] -> [(Index, String)] 
delIndices [] list                 = list
delIndices [(v, indx)] list        = filter (\(x, s) -> x /= indx) list
delIndices ((v, indx):other) list  = filter (\(x, s) -> x /= indx) (delIndices other list)

insIndices :: Index -> String -> Integer -> [(Index, String)] -> [(Index, String)] 
insIndices indx name size list 
  | (size == 0) = list
  | (size > 0)  = insIndices (indx + 1) name (size - 1) (insertOrdered indx name list)
  | otherwise   = [] --it's impossible as size is checked before invocation

insertOrdered :: Ord a => a -> String -> [(a, String)] -> [(a, String)]
insertOrdered a name [] = [(a, name)]
insertOrdered a name list@((b, s):bs)
  | a <= b    = (a, name) : list
  | otherwise = (b, s) : insertOrdered a name bs

getAnyIndex ::[(Index, String)]  -> Index
getAnyIndex []       = 0
getAnyIndex [(a, s)] = a + 1
getAnyIndex ((a, s):bs@((b, s2):oth))
  | (b == a + 1) = getAnyIndex bs
  | otherwise    = a + 1


getSuitableIndex :: [(Index, String)] -> Integer -> Index
getSuitableIndex [] size       = 0
getSuitableIndex [(a, s)] size = a + 1
getSuitableIndex ((a, s1):bs@((b, s2):oth))  size 
  | (b > a + size) = a + 1
  | otherwise      = getSuitableIndex bs size
  
getIndexOfVal :: [(CVal, Index)] -> Expr -> Env -> Throwable Index
getIndexOfVal vals expr env = do
  val <- evalExpr expr env
  case val of
    CInt indx -> do
      (val, ind) <- helperIndex vals indx
      return ind
    _         -> Left (TypeMismatch "Incorrect type of index")
   
getIndexedVal :: [(CVal, Index)] -> Expr -> Env -> Throwable CVal
getIndexedVal vals (Literal CNull) _ = do
  (val, ind) <- helperIndex vals 0
  return val
getIndexedVal vals expr env          = do
  val <- evalExpr expr env
  case val of
    CInt indx -> do
      (val, ind) <- helperIndex vals indx
      return val
    _         -> Left (TypeMismatch "Incorrect type of index")
  
helperIndex :: [(CVal, Index)] -> Integer -> Throwable (CVal, Index)
helperIndex [] _ = Left (Default "Incorrect index call")
helperIndex ((val, ind):other) n 
  | (n > 0)   = helperIndex other (n - 1)
  | (n == 0)  = return (val, ind)
  | otherwise = Left (Default "Incorrect index call")  

changeVal :: [(CVal, Index)] -> CVal -> CVal -> Throwable [(CVal, Index)]
changeVal old indx val = case indx of
  CInt num -> change old num val
  CPtr     -> change old 0 val
  CNull    -> change old 0 val
  _        -> Left (TypeMismatch "Array's index must be numerical")
  
change :: [(CVal, Index)] -> Index -> CVal -> Throwable [(CVal, Index)]
change [] indx val = Left (OutOfRange "Index is out of range")
change (cell@(old, indx):oth) 0 val = return ((val, indx):oth)
change (cell:other) indx val = do
  list <- change other (indx - 1) val
  return $ cell : list