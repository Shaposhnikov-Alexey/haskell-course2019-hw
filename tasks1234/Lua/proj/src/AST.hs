{-# OPTIONS_GHC -fwarn-incomplete-patterns -fwarn-incomplete-uni-patterns #-}

module AST where

data Literal
    = Var    Literal
    | Call   Statement 
    | AConst Double
    | BConst Bool
    | SConst String
    | Nil
    | BiOp   Op Literal Literal
    | UnOp   Op Literal
    deriving (Show, Eq, Ord)

data Op
    = Add | Sub | Mul | Div 
    | Mod | Pow | Not | And | Neg
    | Or  | Eq  | Lt  | Le  | Fld
    | Len | Ne  | Gt  | Ge  | Con
    deriving (Show, Eq, Ord)

data Statement
    = Assign     [Literal] [Either Literal Statement]
    | Block      [Statement]
    | Define     Literal [Literal] Statement
    | For        Literal Literal Literal Literal Statement
    | ForEach    Literal [Literal] Statement
    | Function   Literal [Literal]
    | IfThenElse Literal Statement (Maybe Statement)
    | Lambda     [Literal] Statement [Literal]
    | ALambda    [Literal] Statement
    | Local      Statement
    | Return     Literal
    | Repeat     Statement Literal
    | While      Literal Statement
    
    | Break
    | Goto       Literal
    | Label      Literal
    deriving (Show, Eq, Ord)
