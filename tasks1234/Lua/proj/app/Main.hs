{-# LANGUAGE QuasiQuotes, OverloadedStrings #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns -fwarn-incomplete-uni-patterns #-}

module Main where

import AST
import Parser
import Interpreter
import Optimization
import Text.Megaparsec
import Text.Pretty.Simple (pPrint)
import Text.InterpolatedString.QM (qnb)


main :: IO ()
main = pPrint $ evalProg . dceProg <$> parseMaybe whileParser input
    where
        input  = [qnb|
            --[[Hello, World!]]--
            _init = "Hello, " .. "World!" -- dead code

            --[[functions]]--
            function sqrt(x) return x^(1/2) end -- dead code
            function square(x) return x^2 end
            function sum(x,y) return x+y end
            
            --[[Z-combinator]]--
            res1 = local do 
  
                Z = function (f) return 
                    function (x) return f(function (v) return x(x)(v) end) end
                    (function (x) return f(function (v) return x(x)(v) end) end)
                end

                F = function (g) return 
                        function (n) if n == 0
                            then return 1
                            else return n * g(n - 1)
                        end
                    end

                return Z(F)(7)
            end

            --[[factorial by loop]]--  
            res2, mul =  1, 1
            limit = 6
            while (mul <= limit) do
                res2 = res2 * mul
                mul = mul + 1
            end

            --[[tables]]--
            tab1 = {six = sqrt(36); a = square(3); b = sum(tab1.a, square(4))} --creation

            tab2 = {a = tab1.a; b = tab1.b} --copy

            tab1 = nil --deletion

            if tab1.a == nil    --all elements are deleted too
                then tab2.a = 1 --copy of table exists, tab2.b is 25
                else tab2.a = 0

            return res1 / res2 + tab2.b * tab2.a -- 5040/720 + 25*1
        |]
