module HRuby where

import Data.Char

data Type = BOOL | INT | CHAR | FLOAT | ARRAY | OBJECT | VOID | FUNCTION
    deriving (Show, Eq)

data RValue = VBool { valueBool::Bool } 
            | VInt { valueInt::Int }
            | VChar { valueChar::Char } 
            | VFloat { valueFloat::Float }
            | VArray { valueArray::[RValue] }
            | VFunction {  }
            | VObject { mems::[Binding] }
            | VVoid
    deriving (Show)

getValueType::RValue -> Type 
getValueType (VBool _) = BOOL
getValueType (VInt _) = INT
getValueType (VChar _) = CHAR
getValueType (VFloat _) = FLOAT
getValueType (VArray _) = ARRAY
getValueType (VObject _) = OBJECT
getValueType VVoid = VOID

-- data RCLass = RClass { name::String, fields::[String], methods::[(String, [String])] }

infixl 6 #+ 
(#+)::RValue -> RValue -> RValue
(#+) (VBool a) (VBool b) = VBool (a || b)
(#+) (VInt a) (VInt b) = VInt (a + b)
(#+) (VFloat a) (VFloat b) = VFloat (a + b)
(#+) (VArray a) (VArray b) = VArray (a ++ b)
(#+) _ _ = undefined

infixl 6 #-
(#-)::RValue -> RValue -> RValue
(#-) (VInt a) (VInt b) = VInt (a - b)
(#-) (VFloat a) (VFloat b) = VFloat (a - b)
(#-) _ _ = undefined

infixl 7 #*
(#*)::RValue -> RValue -> RValue
(#*) (VBool a) (VBool b) = VBool (a && b)
(#*) (VInt a) (VInt b) = VInt (a * b)
(#*) (VFloat a) (VFloat b) = VFloat (a * b)
(#*) _ _ = undefined

infixl 7 #/
(#/)::RValue -> RValue -> RValue
(#/) (VInt a) (VInt b) = VInt (a `div` b)
(#/) (VFloat a) (VFloat b) = VFloat (a / b)
(#/) _ _ = undefined

rNegate::RValue -> RValue 
rNegate (VBool a) = VBool (not a)
rNegate (VInt a) = VInt (-a)
rNegate (VFloat a) = VFloat (-a)
rNegate _ = undefined

data Binding = FunBinding  {funName::String, parameters::[String], body::[Instruction] }
             | ClassBinding { className::String, members::[String], methods::[Binding] }
             | VarBinding { varName::String, rValue::RValue } 
    deriving (Show)

type Context = [[Binding]]

lookupVar::Context -> String -> RValue
lookupVar [] name = error "Variable not in scope => NOSCOPE360MLG"
lookupVar ([]:ctx) name = lookupVar ctx name
lookupVar ((bnd@(VarBinding vn vl) : bnds):ctx) name = if (vn == name) then vl else lookupVar (bnds:ctx) name
lookupVar ((bnd:bnds):ctx) name = lookupVar (bnds:ctx) name

lookupFun::Context -> String -> ([String], [Instruction])
lookupFun [] name = error "Function not in scope => NOSCOPE360MLG"
lookupFun ([]:ctx) name = lookupFun ctx name
lookupFun ((bnd@(FunBinding fn fp fb) : bnds):ctx) name = if (fn == name) then (fp, fb) else lookupFun (bnds:ctx) name
lookupFun ((bnd:bnds):ctx) name = lookupFun (bnds:ctx) name

lookupClass::Context -> String -> ([String], [Binding])
lookupClass [] name = error "Class not in scope => NOSCOPE360MLG"
lookupClass ([]:ctx) name = lookupClass ctx name
lookupClass ((bnd@(ClassBinding cn cf cm):bnds):ctx) name = if (cn == name) then (cf, cm) else lookupClass (bnds:ctx) name
lookupClass ((bnd:bnds):ctx) name = lookupClass (bnds:ctx) name


getMember::RValue -> String -> RValue
getMember (VObject members) memName = lookupVar [members] memName
getMember _ _ = error "Class member doesn't exist!"

getMethod::RValue -> String -> ([String], [Instruction])
getMethod (VObject members) memName = lookupFun [members] memName
getMethod _ _ = error "Class member doesn't exist!"


getRetValue ctx (paramNames, body) args = rValue (head (head 
    (callFun ctx (paramNames, body) args)))

callFun ctx (paramNames, body) args = executeProgram 
    ((params ctx paramNames args):ctx) body where
        params :: Context -> [String] -> [Expression] -> [Binding]
        params ctx [] [] = []
        params ctx (nm:nms) (arg:args) = (VarBinding nm (eval ctx arg)):(params ctx nms args)
        params _ _ _ = error "Incorrect number of arguments"

data Expression = Val RValue 
                | Bracketed Expression
                | Expression :+: Expression 
                | Expression :*: Expression 
                | Expression :/: Expression 
                | Expression :-: Expression
                | Neg Expression 
                | Var String 
                | Fun String [Expression]
                | Member Expression String
                | Method Expression String [Expression]
                | Cons String [Expression]
    deriving (Show)


eval::Context -> Expression -> RValue
eval ctx (Bracketed e) = eval ctx e
eval ctx (Neg e) = rNegate (eval ctx e)
eval ctx (Val x) = x
eval ctx (Var s) = lookupVar ctx s
eval ctx (e1 :+: e2) = eval ctx e1 #+ eval ctx e2
eval ctx (e1 :*: e2) = eval ctx e1 #* eval ctx e2
eval ctx (e1 :/: e2) = eval ctx e1 #/ eval ctx e2
eval ctx (e1 :-: e2) = eval ctx e1 #- eval ctx e2
eval ctx (Fun name args) = getRetValue ctx (lookupFun ctx name) args
eval ctx (Member ofObj name) = getMember (eval ctx ofObj) name
eval ctx (Method ofObj name args) = getRetValue ((getMems obj):ctx) (getMethod obj name) args where 
    obj = eval ctx ofObj
eval ctx (Cons name fields) = consObj ctx name fields
eval _ _ = undefined


varBinding::Context -> String -> Expression -> Binding
varBinding ctx name expr = VarBinding {varName = name, rValue = val} where
    val = eval ctx expr

getMems::RValue -> [Binding]
getMems (VObject mems) = mems
getMems _ = error "Can't get members of non-object!"

bindMethod::Context -> String -> String -> [String] -> [Instruction] -> Context
bindMethod ([]:ctx) className mName mArgs mBody = error "Can't add methods to nonexistent class!"
bindMethod ((bnd@(ClassBinding cn fields methods):bnds):ctx) className mName mArgs mBody = if (cn == className) then ((ClassBinding cn fields ((genMethod (mName, mArgs, mBody)):methods)):bnds):ctx else (bnd:bnds'):ctx' where
    (bnds':ctx') = bindMethod (bnds:ctx) className mName mArgs mBody 
bindMethod ((bnd:bnds):ctx) className mName mArgs mBody = (bnd:bnds'):ctx' where
    (bnds':ctx') = bindMethod(bnds:ctx) className mName mArgs mBody 


bindMember::Context -> Context -> [String] -> Expression -> Context
bindMember genCtx ctx [name] expr = bindVar' genCtx ctx name expr
bindMember genCtx ctx (name:rest) expr = bindVar' genCtx ctx name (Val (VObject (head newMems)))  where 
    newMems = bindMember genCtx ((getMems (lookupVar ctx name)):ctx) rest expr

bindVar'::Context -> Context -> String -> Expression -> Context
bindVar' genCtx ([]:ctx) name expr = error "Class member doesn't exist!"
bindVar' genCtx ((bnd@(VarBinding vn _):bnds):ctx) name expr = if (vn == name) then ((varBinding genCtx name expr):bnds):ctx else (bnd:bnds'):ctx' where
    (bnds':ctx') = bindVar' genCtx (bnds:ctx) name expr
bindVar' genCtx ((bnd:bnds):ctx) name expr = (bnd:bnds'):ctx' where
    (bnds':ctx') = bindVar' genCtx (bnds:ctx) name expr


bindVar::Context -> Context -> String -> Expression -> Context
bindVar genCtx ([]:ctx) name expr = [varBinding genCtx name expr]:ctx
bindVar genCtx ((bnd@(VarBinding vn _):bnds):ctx) name expr = if (vn == name) then ((varBinding genCtx name expr):bnds):ctx else (bnd:bnds'):ctx' where
    (bnds':ctx') = bindVar genCtx (bnds:ctx) name expr
bindVar genCtx ((bnd:bnds):ctx) name expr = (bnd:bnds'):ctx' where
    (bnds':ctx') = bindVar genCtx (bnds:ctx) name expr

bindFun :: Context -> Context -> String -> [String] -> [Instruction] -> Context
bindFun genCtx ([]:ctx) name params body = [FunBinding name params body]:ctx
bindFun genCtx ((bnd@(FunBinding fn _ _):bnds):ctx) name params body = if (fn == name) then ((FunBinding name params body):bnds):ctx else (bnd:bnds'):ctx' where
    (bnds':ctx') = bindFun genCtx (bnds:ctx) name params body
bindFun genCtx ((bnd:bnds):ctx) name params body = (bnd:bnds'):ctx' where
    (bnds':ctx') = bindFun genCtx (bnds:ctx) name params body

genMethod (name, params, body) = FunBinding name params body

bindClass::Context -> Context -> String -> [String] -> [(String, [String], [Instruction])] -> Context
bindClass genCtx ([]:ctx) name fields methods = [ClassBinding name fields (map genMethod methods)]:ctx
bindClass genCtx ((bnd@(ClassBinding cn flds mthds):bnds):ctx) name fields methods = if (cn == name) then ((ClassBinding name flds ((map genMethod methods) ++ mthds)):bnds):ctx else (bnd:bnds'):ctx' where
    (bnds':ctx') = bindClass genCtx (bnds:ctx) name fields methods
bindClass genCtx ((bnd:bnds):ctx) name fields methods = (bnd:bnds'):ctx' where
    (bnds':ctx') = bindClass genCtx (bnds:ctx) name fields methods

consObj::Context -> String -> [Expression] -> RValue
consObj ctx className fieldValues = VObject ((zipWith (varBinding ctx) fields fieldValues) ++ methods) where 
    (fields, methods) = lookupClass ctx className

data Instruction = ForLoop { cntName::String, cntValues::[Int], forBody::[Instruction] } 
                 | WhileLoop { whileCond::Expression, whileBody::[Instruction] }
                 | If { ifCond::Expression, ifTrue::[Instruction], ifFalse::[Instruction] }
                 | VarAssign { newName::String, newValue::Expression }
                 | FieldAssign { names::[String], newValue::Expression }
                 | DynAddMethod { targetClassName::String, newMethodName::String, methodParams::[String], methodBody::[Instruction] }
                 | MethodCall { names::[String], args::[Expression] }
                 | FuncDeclare { functionName::String, params::[String], funBody::[Instruction] }
                 | FuncCall { callName::String, args::[Expression] } 
                 | Ret { result::Expression }
                 | ClassDeclare { newClassName::String, newMembers::[String], newMethods::[(String, [String], [Instruction])] } 
    deriving Show

type Program = [Instruction]

executeProgram::Context -> Program -> Context

execute::Context -> Instruction -> Context
execute ctx wl@(WhileLoop {whileCond = cnd, whileBody = body}) = if valueBool (eval ctx cnd) then execute ctx' wl else ctx where
    ctx' = executeProgram ctx body
execute ctx (If { ifCond = cnd, ifTrue = bodyTrue, ifFalse = bodyFalse }) = if valueBool (eval ctx cnd) 
    then executeProgram ctx bodyTrue 
    else executeProgram ctx bodyFalse
execute ctx (VarAssign { newName = s, newValue = expr }) = bindVar ctx ctx s expr
execute ctx (Ret expr) = [(varBinding ctx "retValue" expr)]:(tail ctx)
execute ctx (FuncCall s args) = tail (callFun ctx (lookupFun ctx s) args)
execute ctx (FuncDeclare s params body) = bindFun ctx ctx s params body
execute ctx (FieldAssign names expr) = bindMember ctx ctx names expr
execute ctx (ClassDeclare name fields methods) = bindClass ctx ctx name fields methods
execute ctx (DynAddMethod cn mName mArgs mBody) = bindMethod ctx cn mName mArgs mBody 
execute ctx _ = undefined

executeProgram ctx [] = ctx
executeProgram ctx (instruction:program) = executeProgram (execute ctx instruction) program



 -- Parser

type Stream = [Char]
newtype Parser a = P (Stream -> [(a, Stream)])

run (P f) = f

pReturn x = P $ \s -> [(x, s)]

instance Functor Parser where
    fmap f (P p) = P $ fmap (fmap (first f)) p where
        first f (a, b) = (f a, b)

apply :: Parser (a -> b) -> Parser a -> Parser b
apply (P pf) (P px) = P $ \inp ->
    flip concatMap (pf inp) $ \ (f, tl1) ->
    flip concatMap (px tl1) $ \ (x, tl2) ->
    [(f x, tl2)]

instance Applicative Parser where
    pure = pReturn
    (<*>) = apply

pFail = P (const [])

alt (P p1) (P p2) = P (\ inp -> p1 inp ++ p2 inp)

altOr (P p1) (P p2) = P (\ inp -> 
    if length (p1 inp) > 0 then p1 inp else p2 inp)

instance Monad Parser where
    return = pReturn
    
    P pa >>= a2pb = P $ \inp -> 
        flip concatMap (pa inp) $ \(x, tl) ->
        run (a2pb x) tl

pSym :: Char -> Parser Char
pSym x = P helper where
    helper [] = []
    helper (c:s) = if c == x then [(c, s)] else []

pLetter :: Parser Char
pLetter = P helper where
    helper [] = []
    helper (c:s) = if isLetter c then [(c, s)] else []

pDigit :: Parser Char
pDigit = P helper where
    helper [] = []
    helper (c:s) = if isDigit c then [(c, s)] else []


-- Parser of Letters, Digits and Underscores
pLDU :: Parser Char
pLDU = P helper where
    helper [] = []
    helper (c:s) = if isLetter c || isDigit c || c == '_'
        then [(c, s)] 
        else []    

pMany p = do
    l <- p
    r <- altOr (pMany p) (pReturn [])
    return (l:r)

pAny p = altOr (pMany p) (pReturn [])
  
pLDUs = pAny pLDU

pIdentifier = do
    l <- pLetter
    r <- pLDUs
    return (l:r)

pVariable = do
    name <- pIdentifier
    return (Var name)

isEmptyMember::Expression -> Bool
isEmptyMember (Member _ "") = True
isEmptyMember _ = False

pMember left = do
    _ <- pSym '.'
    name <- pIdentifier
    more <- altOr (altOr (pMethodCall (Member left name))
        (pMember (Member left name)))
        (pReturn (Member left ""))
    return (if (isEmptyMember more) then (Member left name) 
        else more)

pMethodCall left = do
    _ <- pSym '.'
    name <- pIdentifier
    _ <- pSym '('
    args <- altOr (pListOf pExpr) (pReturn [])
    _ <- pSpaces
    _ <- pSym ')'
    more <- altOr (altOr (pMethodCall (Method left name args))
        (pMember (Method left name args)))
        (pReturn (Member left ""))
    return (if (isEmptyMember more) then 
        (Method left name args) else more)

pMembersExp = do
    left <- altOr (altOr pCons pFuncCall) pVariable
    mems <- altOr (altOr (pMethodCall left) (pMember left))
        (pReturn (Member left ""))
    return (if (isEmptyMember mems) then left else mems)

pIdHelper = do
    id <- pIdentifier
    return [id]

pMemberAss = do
    left <- pIdentifier
    _ <- pSym '.'
    right <- altOr pMemberAss pIdHelper
    return (left:right)

pCons = do
    name <- pIdentifier
    _ <- pSym '.'
    _ <- pString "new"
    _ <- pSym '('
    args <- altOr (pListOf pExpr) (pReturn [])
    _ <- pSpaces
    _ <- pSym ')'
    return (Cons name args)



intFromString = foldl (\a b -> 10 * a + digitToInt b) 0
    
pInt = do
    val <- pMany pDigit
    return (VInt (intFromString val))

pow10 0 = 1
pow10 n | n > 0 = 10 * pow10 (n - 1)
pow10 n | n < 0 = 1

pFloatForced = do
    l <- pMany pDigit
    r <- pFrac
    return (VFloat (fromIntegral (intFromString l) + r)) where
        pFrac = do
            _ <- pSym '.'
            r <- pMany pDigit
            return (fromIntegral (intFromString r) /
                fromIntegral (pow10 (length r)))


pSpaces = pAny (pSym ' ')

pNotSym x = P helper where
    helper [] = []
    helper (c:s) = if c == x then [] else [(c, s)]

pCharConst = do
    _ <- pSym '"'
    c <- pNotSym '"'
    _ <- pSym '"'
    return (VChar c)

pStringConst = do
    _ <- pSym '"'
    s <- pAny (pNotSym '"')
    _ <- pSym '"'
    return (VChar 'a') --('"' : (s ++ ['"']))

pTrue = do
    _ <- pString "true"
    return (VBool True)

pFalse = do
    _ <- pString "false"
    return (VBool False)

pBool = alt pTrue pFalse

pConst = do
    res <- alt pBool (alt pCharConst(altOr pFloatForced pInt))
    return (Val res)

genExpr exp1 sgn exp2 | sgn == '&' = exp1 :*: exp2
                      | sgn == '|' = exp1 :+: exp2
                      | sgn == '*' = exp1 :*: exp2
                      | sgn == '/' = exp1 :/: exp2
                      | sgn == '+' = exp1 :+: exp2
                      | sgn == '-' = exp1 :-: exp2

pBracketed ops = do
    _ <- pSym '('
    _ <- pSpaces
    expr <- pExpr
    _ <- pSpaces
    _ <- pSym ')'
    return (if length ops == 0 then expr else 
        genExpr exprL sgn expr) where
            (exprL, sgn) = head ops

pNeg ops = do
    sgn <- alt (pSym '-') (pSym '!')
    _ <- pSpaces
    expr <- pExpr
    return (if length ops == 0 then Neg expr else 
        genExpr exprL sgn (Neg expr)) where
            (exprL, sgn) = head ops


pSingleExp ops = do
    _ <- pSpaces
    val <- altOr pConst pMembersExp
    _ <- pSpaces
    return (if length ops == 0 then val else 
        genExpr exprL sgn val) where
            (exprL, sgn) = head ops

pProd ops = do
    exprL <- altOr (pSingleExp []) (pBracketed [])
    _ <- pSpaces
    sgn <- alt (pSym '&') (alt (pSym '*') (pSym '/'))
    _ <- pSpaces
    exprR <- altOr (pProd (nops exprL sgn))(altOr 
        (pSingleExp (nops exprL sgn)) 
        (pBracketed (nops exprL sgn)))
    return exprR where
                nops exprL sgn = if length ops == 0 then
                    [(exprL, sgn)] else
                    (genExpr exprLn nsgn exprL, sgn):(tail ops) 
                        where (exprLn, nsgn) = head ops

pProd' ops = do
    expr <- pProd []
    return (if length ops == 0 then expr else 
        genExpr exprL sgn expr) where
            (exprL, sgn) = head ops

pSum ops = do
    exprL <- altOr (altOr (pProd []) (pNeg []))
        (altOr (pSingleExp []) (pBracketed []))
    _ <- pSpaces
    sgn <- alt (pSym '|') (alt (pSym '+') (pSym '-'))
    _ <- pSpaces
    exprR <- altOr (pNeg (nops exprL sgn)) 
        (altOr (altOr (pSum (nops exprL sgn)) 
        (pProd' (nops exprL sgn)))
        (altOr (pSingleExp (nops exprL sgn)) 
        (pBracketed (nops exprL sgn))))
    return exprR where
                nops exprL sgn = if length ops == 0 then
                    [(exprL, sgn)] else
                    (genExpr exprLn nsgn exprL, sgn):(tail ops) 
                        where (exprLn, nsgn) = head ops


pExpr :: Parser Expression
pExpr = altOr (pProd [])
     (altOr (altOr (pNeg []) (pSum []))
     (altOr (pSum []) (pSingleExp [])))

pFuncCall = do
    l <- pIdentifier
    _ <- pSym '('
    args <- altOr (pListOf pExpr) (pReturn [])
    _ <- pSpaces
    _ <- pSym ')'
    return (Fun l args)

pRet = do
    _ <- pString "return"
    _ <- pSpaces
    expr <- pExpr
    return (Ret expr)

pIndent :: Int -> Parser Char
pIndent 0 = return ' '
pIndent 1 = pSym ' '
pIndent k = do
    _ <- pSym ' '
    ret <- pIndent (k - 1)
    return ret

pFieldAss = do 
    lst <- pMemberAss
    _ <- pSpaces
    _ <- pSym '='
    _ <- pSpaces
    r <- pExpr
    return (FieldAssign lst r)


pVarAss = do
    l <- pIdentifier
    _ <- pSpaces
    _ <- pSym '='
    _ <- pSpaces
    r <- pExpr
    return (VarAssign l r)

pAss = altOr pFieldAss pVarAss

pString [] = return []
pString (c:s) = do
    h <- pSym c
    t <- pString s
    return (h:t)

pListOf p = do
    _ <- pSpaces
    l <- p
    _ <- pSpaces
    r <- altOr (pRest p) (pReturn [])
    return (l:r) where
        pRest p = do
            _ <- pSym ','
            _ <- pSpaces
            res <- pListOf p
            return res

pFunDeclare indent = do
    _ <- pString "def"
    _ <- pSpaces
    name <- pIdentifier
    _ <- pSym '('
    args <- pListOf pIdentifier
    _ <- pSpaces
    _ <- pSym ')'
    _ <- pSpaces
    _ <- pSym '\n'
    prog <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "end"
    return (FuncDeclare name args prog)

pMethod indent = do
    _ <- pIndent indent
    _ <- pString "def"
    _ <- pSpaces
    name <- pIdentifier
    _ <- pSym '('
    args <- pListOf pIdentifier
    _ <- pSpaces
    _ <- pSym ')'
    _ <- pSpaces
    _ <- pSym '\n'
    prog <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "end"
    _ <- pSpaces
    _ <- pSym '\n'
    return (name, args, prog)


pField indent = do
    _ <- pIndent indent
    name <- pIdentifier
    _ <- pSpaces
    _ <- pSym '\n'
    return name

pClassDeclare = do
    _ <- pString "class"
    _ <- pSpaces
    name <- pIdentifier
    _ <- pSpaces
    _ <- pSym '\n'
    fields <- pAny (pField 4)
    methods <- pAny (pMethod 4)
    _ <- pString "end"
    return (ClassDeclare name fields methods)


pWhile indent = do
    _ <- pString "while"
    _ <- pSpaces
    cond <- pExpr
    _ <- pSpaces
    _ <- altOr (pString "do") (pReturn [])
    _ <- pSpaces
    _ <- pSym '\n'
    body <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "end"
    return (WhileLoop cond body)

pIf indent = do
    _ <- pString "if"
    _ <- pSpaces
    cond <- pExpr
    _ <- pSpaces
    _ <- pSym '\n'
    bodyT <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "else"
    _ <- pSpaces
    _ <- pSym '\n'
    bodyF <- pProg (indent + 4)
    _ <- pIndent indent
    _ <- pString "end"
    return (If cond bodyT bodyF)


pInstr indent = altOr
    (pFunDeclare indent)
    (altOr pAss pRet)

pProg indent = do
    _ <- pIndent indent
    l <- pInstr indent
    _ <- pSpaces
    _ <- pSym '\n'
    r <- altOr (pProg indent) (pReturn [])
    return (l:r)