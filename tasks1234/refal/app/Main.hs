module Main where

import RefalAst
import RefalParser (refalScope)
import RefalInterpreter (interpret, test)

import Text.Megaparsec (runParser)
import Text.Pretty.Simple (pPrint)

main :: IO ()
main = do
    p <- runParser refalScope "app/pal" <$> readFile "app/pal"
    case p of
        Left e -> pPrint e
        Right ast -> do
            pPrint $ test ast
            pPrint $ interpret ast
