module Parse where

import Prelude hiding (Ordering)
import Data.Functor.Identity
import Data.Char
import Data.Void
import Text.Megaparsec
import qualified Text.Megaparsec.Char.Lexer as L
import Control.Monad.Combinators.Expr
import Text.Megaparsec.Char
import Data.List.NonEmpty (NonEmpty, fromList)
import Control.Applicative ((<$>),(*>),(<*>))
import Control.Monad (void,guard)
import Data.List hiding (insert)
import Control.Monad (void)

import AST

type Parser = Parsec Void String

integer :: Parser Expr
integer = LiteralValue <$> NumLiteral <$> lexeme L.decimal

stringLiteral :: Parser Expr
stringLiteral = LiteralValue <$> StringLiteral
  <$> lexeme (char '\'' *> manyTill L.charLiteral (char '\''))

columnNameLiteral :: Parser Expr
columnNameLiteral = ColumnNameLiteral <$> lexeme
  ((:) <$> letterChar <*> many alphaNumChar)

uInt :: Parser UInt
uInt = lexeme $ do
    x <- digitChar
    xs <- many $ digitChar
    return $ UInt $ read $ x:xs
    
stringLetter :: Parser Char
stringLetter = satisfy (/= '\'')

stringEscape :: Parser (Maybe Char)
stringEscape = string "\\'" *> return (Just '\'') 

stringChar :: Parser (Maybe Char)
stringChar = do { c <- stringLetter; return (Just c) }
          <|> stringEscape

varChar :: Parser String
varChar = lexeme $ do 
    str <- between (char '\'') (char '\'') (many stringChar)
    return (foldr (maybe id (:)) "" str)

parens :: Parser a -> Parser a
parens = between (symbol "(") (symbol ")")

sc :: Parser ()
sc = L.space
  space1        
  (L.skipLineComment "--")       
  (L.skipBlockComment "/*" "*/")

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

symbol :: String -> Parser String
symbol = L.symbol sc

comma :: Parser ()
comma = lexeme $ void $ char ','

commaSeparated :: Parser a -> Parser (NonEmpty a)
commaSeparated a = lexeme $ sepBy1 a comma >>= return . fromList

matched :: Char -> Parser a -> Char -> Parser a
matched o a c = open *> lexeme a <* close
    where
        open = lexeme $ char o
        close = lexeme $ char c

keywords :: [String]
keywords = sort $ map (map toLower)
    [ "AS", "FROM", "TABLE", "SELECT", "INSERT", "INTO", "REMOVE", "UPDATE", "SET", "VALUES"
    , "NULL", "JOIN", "GROUP", "BY", "LIMIT", "ORDER", "NOT", "CREATE", "AND", "OR", "WHERE"
    ]

isKeyword :: String -> Bool
isKeyword ident = find keywords
    where
        find [] = False
        find (x:xs) = case (compare x ident) of
            LT -> find xs
            EQ -> True
            GT -> False  

reserved :: String -> Parser ()
reserved xs = lexeme $
    if isKeyword xs
        then string' xs >> notFollowedBy alphaNumChar >> return ()
        else error $ xs ++ " is not a keyword"

reservedSeq :: [String] -> Parser ()
reservedSeq [] = return ()
reservedSeq (x:xs) = reserved x >> reservedSeq xs

definedName :: Parser DefinedName
definedName = lexeme $ try $ do
    x  <- letterChar
    xs <- many $ alphaNumChar
    let name = x:xs
    case isKeyword name of
        True -> unexpected . Label $ fromList $ "reserved keyword: " ++ name
        False -> return $ DefinedName name

tableId :: Parser TableId
tableId = lexeme $ try definedName >>= return . TableId

columnId :: Parser ColumnId
columnId = lexeme $ try definedName >>= return . ColumnId

tableName :: Parser TableName
tableName = lexeme $ try tableId >>= return . TableName

columnName :: Parser ColumnName
columnName = lexeme $ do
    table  <- optional $ try $ tableName >>= \x -> char '.' >> return x
    column <- columnId
    return $ ColumnName table column

sortOrdering :: Parser Ordering
sortOrdering = lexeme $ choice
    [ try $ reserved "asc" >> return Asc 
    , reserved "desc" >> return Desc
    ]

toType :: [(String, a)] -> Parser a
toType = choice . map (\(t, x) -> string' t >> return x)

expr :: Parser Expr
expr = makeExprParser term operatorTable

operatorTable :: [[Operator Parser Expr]]
operatorTable =
  [ [ prefix "-"  Negation
    , prefix "+"  id
    ]
  , [ binary "*"  Product
    , binary "/"  Division
    ]
  , [ binary "+"  Sum
    , binary "-"  Subtr
    ]
  , [ binary "="  Eq
    , binary "<>" NotEq
    ]
  , [ binary "<=" LE
    , binary ">=" GE
    ]
  , [ binary "<"  Less
    , binary ">"  Gr
    ]
  , [ prefix "NOT" Not
    ]
  , [ binary "AND" And
    , binary "OR"  Or
    ]
  ]

binary :: String -> (Expr -> Expr -> Expr) -> Operator Parser Expr
binary  name f = InfixL  (f <$ symbol name)

prefix, postfix :: String -> (Expr -> Expr) -> Operator Parser Expr
prefix  name f = Prefix  (f <$ symbol name)
postfix name f = Postfix (f <$ symbol name)

term :: Parser Expr
term = choice
  [ parens expr
  , columnNameLiteral
  , stringLiteral
  , integer
  ]

selectedTable :: Parser SelectedTable
selectedTable = lexeme $ try tableName >>= return . SelectedTable

selectedTablesList :: Parser SelectedTablesList
selectedTablesList = lexeme $ do
    tables <- try $ commaSeparated selectedTable
    return $ SelectedTablesList tables

selectSubList :: Parser SelectSubList
selectSubList = SelectSubList <$> expr

star :: Parser ()
star = lexeme $ void $ char '*'

selectList :: Parser SelectList
selectList = try $ SelectList <$> commaSeparated selectSubList
          <|> star *> return SelectAll 

select :: Parser Select
select = Select
      <$> (reserved "select" >> Parse.selectList)
      <*> (reserved "from" >> Parse.selectedTablesList)
      <*> try (optional $ reserved "where" >> searchCondition)

searchCondition :: Parser SearchCondition
searchCondition = SearchCondition <$> expr

parseType :: Parser Type
parseType = lexeme $ toType [ ("varchar", VarChar) ] <*> matched '(' uInt ')'
         <|> toType [ ("int", Int) ]

columnAndType :: Parser ColumnAndType
columnAndType = ColumnAndType <$> columnId <*> parseType

create :: Parser Create
create = Create <$> (reservedSeq ["create", "table"] >> tableName)
      <*> matched '(' (commaSeparated columnAndType) ')'

remove :: Parser Remove
remove = Remove
      <$> (reservedSeq ["remove", "from"] >> tableName)
      <*> try (optional $ reserved "where" >> searchCondition)

insert :: Parser Insert
insert = Insert
      <$> (reservedSeq ["insert", "into"] >> tableName)
      <*> optional (matched '(' (commaSeparated columnId) ')')
      <*> (reserved "values" >> matched '(' (commaSeparated insertValue) ')')

insertValue :: Parser InsertValue
insertValue = choice
    [ InsertInt <$> integer
    , InsertVarchar <$> varChar
    , reserved "null" >> return InsertNull
    ]

update :: Parser Update 
update = Update 
      <$> (reserved "update" >> tableName)
      <*> (reserved "set" >> updateColumnList) 
      <*> try (optional $ reserved "where" >> searchCondition)
    
updateColumnList :: Parser UpdateColumnList
updateColumnList = commaSeparated updateColumn

updateColumn :: Parser UpdateColumn 
updateColumn = UpdateColumn <$> columnId <* lexeme (char '=') <*> updateValue

updateValue :: Parser UpdateValue 
updateValue = try $ UpdateValueExpr <$> expr
           <|> ((reserved "null") >> return UpdateNull)
    
statement :: Parser Statement
statement =  try (CreateTable <$> create)
         <|> try (SelectSmth <$> select)
         <|> try (InsertSmth <$> insert)
         <|> try (UpdateSmth <$> update)
         <|> RemoveSmth <$> remove

statementList :: Parser StatementList
statementList = StatementList <$> (sepBy1 statement semi >>= return . fromList)
  where
    semi = lexeme $ char ';' 