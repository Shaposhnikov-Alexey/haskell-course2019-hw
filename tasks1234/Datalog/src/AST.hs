module AST where


data Statement = 
      Assertion Clause
    | Retraction Clause
    | Query Literal


data Clause = 
      Clause { head :: Literal, conditions :: [Literal] }


data Literal = 
      Eq { left :: Term, right :: Term }
    | Neq { left :: Term, right :: Term }
    | Predicate { name :: Name, terms :: [Term] }
    
    
data Term = 
      Variable Name
    | Constant Name
    
    
data Name = Name String
